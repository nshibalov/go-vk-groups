package main

import (
	"fmt"
	"log"

	"gitlab.com/nshibalov/go-vk-groups/watcher"
)

func main() {
	w, err := watcher.NewVKWatcher()
	if err != nil {
		log.Fatal(err)
	}

	w.SetCallback(func(eventType watcher.EventType, group *watcher.Group) {
		if group.Parent == nil {
			switch eventType {
			case watcher.EventAdded:
				log.Printf("group '%s' added [%s]", group.Name, group.ModTime)
			case watcher.EventRemoved:
				log.Printf("group '%s' removed [%s]", group.Name, group.ModTime)
			}

			return
		}

		name := fmt.Sprintf("%s/%s", group.Parent.Name, group.Name)

		switch eventType {
		case watcher.EventAdded:
			log.Printf("sub group '%s' added [%s] people: %v", name, group.ModTime, group.People)
		case watcher.EventModified:
			log.Printf("sub group '%s' modified [%s] people: %v", name, group.ModTime, group.People)
		case watcher.EventRemoved:
			log.Printf("sub group '%s' removed [%s]", name, group.ModTime)
		}
	})

	errChan := w.Watch("./example")

	for err := range errChan {
		log.Fatal(err)
	}

	log.Println("done")
}
