package watcher

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
)

type EventType int

const (
	EventAdded EventType = iota
	EventRemoved
	EventModified
)

type Group struct {
	Parent *Group

	Path    string
	Exists  bool
	Name    string
	ModTime time.Time

	People []string // only for subGroup
}

type CallbackFunc func(eventType EventType, group *Group)

type VKWatcher interface {
	Stop()
	Watch(dir string) <-chan error
	SetCallback(callback CallbackFunc)
}

type vkwatcher struct {
	m sync.RWMutex

	rootWatcher  *fsnotify.Watcher
	groupWatcher *fsnotify.Watcher

	paths      map[string]*Group
	extensions []string

	root string
	stop bool

	callback CallbackFunc
}

func (w *vkwatcher) readPeopleFile(path string) ([]string, error) {
	isExtOk := false
	fileExt := strings.ToLower(filepath.Ext(path))

	for _, ext := range w.extensions {
		if strings.EqualFold(fileExt, ext) {
			isExtOk = true
			break
		}
	}

	if !isExtOk {
		return nil, nil
	}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	r := csv.NewReader(bytes.NewReader(data))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	people := make([]string, 0)

	for _, record := range records {
		people = append(people, record...)
	}

	return people, nil
}

func (w *vkwatcher) groupFromPath(path string) (*Group, error) {
	path = filepath.Clean(path)

	if group, ok := w.paths[path]; ok {
		return group, nil
	}

	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		return &Group{
			Path:    path,
			Exists:  false,
			Name:    filepath.Base(path),
			ModTime: stat.ModTime(),
		}, nil
	} else if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		return nil, fmt.Errorf("group path must be dir")
	}

	if err := w.groupWatcher.Add(path); err != nil {
		return nil, fmt.Errorf("error adding sub dir: %s", err)
	}

	return &Group{
		Path:    path,
		Exists:  true,
		Name:    stat.Name(),
		ModTime: stat.ModTime(),
	}, nil
}

func (w *vkwatcher) subGroupFromPath(path string) (*Group, error) {
	path = filepath.Clean(path)

	group, err := w.groupFromPath(filepath.Dir(path))
	if err != nil {
		return nil, err
	}

	groupName := func() string {
		groupName := filepath.Base(path)
		ext := filepath.Ext(path)
		return strings.TrimSuffix(groupName, ext)
	}

	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		return &Group{
			Parent:  group,
			Path:    path,
			Exists:  false,
			Name:    groupName(),
			ModTime: time.Now(),
		}, nil
	} else if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		people, err := w.readPeopleFile(path)
		if err != nil {
			return nil, err
		}

		return &Group{
			Parent:  group,
			Path:    path,
			Exists:  true,
			Name:    groupName(),
			ModTime: stat.ModTime(),
			People:  people,
		}, nil
	}

	return nil, nil
}

func (w *vkwatcher) readGroup(dir string) []*Group {
	ret := make([]*Group, 0)

	_ = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if path == dir {
			return nil
		}

		subGroup, err := w.subGroupFromPath(path)
		if err != nil {
			return err
		}

		if subGroup != nil {
			ret = append(ret, subGroup)
		}

		return nil
	})

	return ret
}

func (w *vkwatcher) addGroup(path string) error {
	group, err := w.groupFromPath(path)
	if err != nil {
		return err
	}

	if !group.Exists {
		return fmt.Errorf("path '%s' doesn't exist", path)
	}

	w.paths[path] = group

	if w.callback != nil {
		ret := w.readGroup(path)

		for _, subGroup := range ret {
			w.callback(EventAdded, subGroup)
		}
	}

	return nil
}

func (w *vkwatcher) removeGroup(path string) error {
	path = filepath.Clean(path)

	if group, ok := w.paths[path]; ok {
		if err := w.groupWatcher.Remove(path); err != nil {
			return fmt.Errorf("error removing sub dir: %s", err)
		}

		delete(w.paths, path)

		if w.callback != nil {
			group.ModTime = time.Now()
			w.callback(EventRemoved, group)
		}
	}

	return nil
}

func (w *vkwatcher) checkGroupEvent(event fsnotify.Event) error {
	if event.Name == w.root {
		// skip root dir changes
		return nil
	}

	switch {
	case event.Op&fsnotify.Create == fsnotify.Create:
		if err := w.addGroup(event.Name); err != nil {
			return err
		}
	case event.Op&fsnotify.Rename == fsnotify.Rename, event.Op&fsnotify.Rename == fsnotify.Remove:
		if err := w.removeGroup(event.Name); err != nil {
			return err
		}
	}

	return nil
}

func (w *vkwatcher) checkSubGroupEvent(event fsnotify.Event) error {
	if _, ok := w.paths[event.Name]; ok { // group deleted
		return nil
	}

	subGroup, err := w.subGroupFromPath(event.Name)
	if err != nil {
		return err
	}

	if subGroup == nil {
		return nil // skip dir
	}

	switch {
	case event.Op&fsnotify.Remove == fsnotify.Remove, event.Op&fsnotify.Rename == fsnotify.Rename:
		w.callback(EventRemoved, subGroup)
	case event.Op&fsnotify.Create == fsnotify.Create:
		w.callback(EventAdded, subGroup)
	case event.Op&fsnotify.Write == fsnotify.Write:
		w.callback(EventModified, subGroup)
	}

	return nil
}

func (w *vkwatcher) loop(errChan chan<- error) {
	defer func() { close(errChan) }()

	for {
		w.m.RLock()
		if w.stop {
			break
		}
		w.m.RUnlock()

		select {
		case event, ok := <-w.rootWatcher.Events:
			if !ok {
				return
			}

			if err := w.checkGroupEvent(event); err != nil {
				errChan <- err
				return
			}
		case err, ok := <-w.rootWatcher.Errors:
			if ok {
				errChan <- err
			}

			return
		// group
		case event, ok := <-w.groupWatcher.Events:
			if !ok {
				return
			}

			if err := w.checkSubGroupEvent(event); err != nil {
				errChan <- err
				return
			}
		case err, ok := <-w.groupWatcher.Errors:
			if ok {
				errChan <- err
			}

			return
		}
	}
}

func (w *vkwatcher) Stop() {
	w.m.Lock()
	defer w.m.Unlock()

	w.stop = true

	w.rootWatcher.Close()
}

func (w *vkwatcher) Watch(dir string) <-chan error {
	dir = filepath.Clean(dir)

	w.root = dir

	w.m.Lock()
	defer w.m.Unlock()

	errChnan := make(chan error)

	stat, err := os.Stat(dir)
	if err != nil {
		errChnan <- err
		return errChnan
	}

	if !stat.IsDir() {
		errChnan <- fmt.Errorf("'%s' is not a dir", dir)
		return errChnan
	}

	// init
	_ = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if path == dir {
			return nil
		}

		stat, err := os.Stat(path)
		if err != nil {
			return err
		}

		if stat.IsDir() {
			return w.addGroup(path)
		}

		return nil
	})

	if err := w.rootWatcher.Add(dir); err != nil {
		errChnan <- err
		return errChnan
	}

	go w.loop(errChnan)

	return errChnan
}

func (w *vkwatcher) SetCallback(callback CallbackFunc) {
	w.callback = callback
}

func NewVKWatcher(extensions ...string) (VKWatcher, error) {
	w, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	groupWatcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	if len(extensions) == 0 {
		extensions = []string{".csv", ".txt"}
	}

	for i, ext := range extensions {
		extensions[i] = strings.ToLower(ext)
	}

	return &vkwatcher{
		rootWatcher:  w,
		groupWatcher: groupWatcher,
		paths:        make(map[string]*Group),
		extensions:   extensions,
	}, nil
}
